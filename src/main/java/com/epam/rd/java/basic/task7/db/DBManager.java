package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;


public class DBManager {

	private static DBManager instance;
	private Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try (InputStream in = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(in);
			System.out.println(properties.getProperty("connection.url"));
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (SQLException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM users";
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				User user = new User();
				user.setId(result.getInt("id"));
				user.setLogin(result.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("", e);
		}
 		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try {
			Statement statement = connection.createStatement();
			String query = String.format("INSERT INTO users (login) VALUES ('%s')", user.getLogin());
			return statement.execute(query);
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}
	public boolean deleteUsers(User... users) throws DBException {
		try {
			Statement statement = connection.createStatement();
			for (User user : users) {
				String query = String.format("DELETE FROM users WHERE login = '%s'",
						user.getLogin());
				statement.execute(query);
			}
			return true;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public User getUser(String login) throws DBException {
		try {
			Statement statement = connection.createStatement();
			String query = String.format("SELECT * FROM users WHERE login = '%s'", login);
			ResultSet result = statement.executeQuery(query);

			if (result.next()) {
				User user = new User();
				user.setId(result.getInt("id"));
				user.setLogin(result.getString("login"));
				return user;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try {
			Statement statement = connection.createStatement();
			String query = String.format("SELECT * FROM teams WHERE name = '%s'", name);
			ResultSet result = statement.executeQuery(query);

			if (result.next()) {
				Team team = new Team();
				team.setId(result.getInt("id"));
				team.setName(result.getString("name"));
				return team;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM teams";
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				Team team = new Team();
				team.setId(result.getInt("id"));
				team.setName(result.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			Statement statement = connection.createStatement();
			String query = String.format("INSERT INTO teams (name) VALUES ('%s')", team.getName());
			statement.execute(query);
			team.setId(getTeam(team.getName()).getId());
			return true;
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams.length == 0) {
			return false;
		}

		try {
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(1);
			Statement statement = connection.createStatement();
			for (Team team : teams) {
				if (team == null) {
					connection.rollback();
					return false;
				}
				String query = String.format("INSERT INTO users_teams (user_id, team_id) VALUES (%d, %d)",
						getUser(user.getLogin()).getId(), getTeam(team.getName()).getId());
				statement.execute(query);
			}
			connection.commit();
			statement.close();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ignored) {}
			throw new DBException("", e);
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				throw new DBException("", e);
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			String query = String.format("SELECT t.id, t.name from teams t inner join users_teams ut ON ut.user_id = %d and t.id = ut.team_id",
					getUser(user.getLogin()).getId());
			ResultSet result = statement.executeQuery(query);

			while (result.next()) {
				Team team = new Team();
				team.setId(result.getInt("id"));
				team.setName(result.getString("name"));
				userTeams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("", e);
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null) {
			return false;
		}
		try {
			Statement statement = connection.createStatement();
			String query = String.format("DELETE FROM teams WHERE name = '%s'",
						team.getName());
			return statement.execute(query);
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			Statement statement = connection.createStatement();
			String query = String.format("UPDATE teams SET name = '%s' WHERE id = %d",
					team.getName(), team.getId());
			return statement.execute(query);
		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}
}
